package tr.com.ykb.service;
import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import static org.mockito.Mockito.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;
import tr.com.ykb.data.*;
import tr.com.ykb.dto.AnnualLeaveRequestDto;
import tr.com.ykb.dto.AnnualLeaveRequestResponseDto;
import tr.com.ykb.model.*;
@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class WorkingDayServiceTest {

    @Mock
    private RestTemplate restTemplate;

    @InjectMocks
    private WorkingDayService workingDayService;

    List<Date> holidaysInTwoYear;
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");

    @Before
    public void init() throws ParseException {
        holidaysInTwoYear=new ArrayList<>();
        holidaysInTwoYear.add(sdf.parse("2019-10-29"));
    }

    @Test
    public void testGetWorkingDayCount() throws URISyntaxException, ParseException {

        Date start=sdf.parse("2019-10-24");
        Date end=sdf.parse("2019-10-30");
        workingDayService.setHolidaysInTwoYear(this.holidaysInTwoYear);

        assertEquals(4,workingDayService.getWorkingDayCount(start,end));
    }
}
