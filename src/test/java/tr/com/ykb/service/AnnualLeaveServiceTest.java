package tr.com.ykb.service;

import static org.junit.Assert.*;
import java.util.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import tr.com.ykb.data.*;
import tr.com.ykb.dto.AnnualLeaveRequestDto;
import tr.com.ykb.dto.AnnualLeaveRequestResponseDto;
import tr.com.ykb.model.*;

@ActiveProfiles("test")
@RunWith(SpringJUnit4ClassRunner.class)
public class AnnualLeaveServiceTest {

    @Mock
    private EmployeeDao employeeDao;

    @Mock
    private AnnualLeaveRuleDao annualLeaveRuleDao;

    @Mock
    private WorkingDayService workingDayService;

    @Mock
    private AnnualLeaveRequestDao annualLeaveRequestDao;

    @Mock
    private AnnualLeaveTransactionDao annualLeaveTransactionDao;

    @Mock
    private AnnualLeaveTransactionDaoImpl annualLeaveTransactionDaoImpl;
    @InjectMocks
    private AnnualLeaveService annualLeaveService;

    @Test
    public void testRequestAnnualService() {
        AnnualLeaveRequestDto req = new AnnualLeaveRequestDto();
        req.setEmployeeID(1l);
        req.setStartDate(new Date());
        Calendar cal = GregorianCalendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, 3);
        req.setEndDate(cal.getTime());

        Employee emp = new Employee();
        emp.setId(34L);
        when(employeeDao.findByID(any(Long.class))).thenReturn(emp);

        when(workingDayService.getWorkingDayCount(any(Date.class), any(Date.class))).thenReturn(2);
        when(annualLeaveTransactionDao.getTotalLeaveAmountByEmployeeID(any(Long.class))).thenReturn(15L);
        when(annualLeaveTransactionDaoImpl.getTotalLeaveAmountByEmployeeID(any(Long.class))).thenReturn(15L);

        when(annualLeaveRequestDao.hasOverlapRequestDate(any(Date.class), any(Date.class), any(Long.class)))
                .thenReturn(false);

        assertNotNull(annualLeaveService.requestAnnualService(req));
    }

    @Test
    public void testUpdateEmployeeAnnualLeaves() {
        List<Employee> employeeList = new ArrayList<>();
        Employee emp = new Employee();
        emp.setFirstName("Onur");
        emp.setLastName("Test");
        emp.setId(1l);
        emp.setStartDate(new Date());
        employeeList.add(emp);
        when(employeeDao.getAnnualLeaveMustBeCheckList()).thenReturn(employeeList);
        AnnualLeaveRule rule = new AnnualLeaveRule();
        rule.setAnnualLeaveDayCount(14);
        when(annualLeaveRuleDao.findByWorkYear(anyInt())).thenReturn(rule);
        annualLeaveService.updateEmployeeAnnualLeaves();
        Mockito.verify(employeeDao, times(1)).update(emp);
    }

    @Test
    public void testGetWaitingAnnualLeaveRequestList(){
        Employee emp = new Employee();
        emp.setId(1l);
        Department dep=new Department();
        dep.setId(2l);
        Employee manager = new Employee();
        manager.setId(2l);
        dep.setManager(manager);
        emp.setDepartment(dep);

        when(employeeDao.findByID(any(Long.class))).thenReturn(emp);

        List<AnnualLeaveRequest> requestList = new ArrayList<>();
        requestList.add(new AnnualLeaveRequest());

        when(annualLeaveRequestDao.findWaitingRequestListByDepartmentID(anyLong())).thenReturn(requestList);

        assertEquals(1,annualLeaveService.getWaitingAnnualLeaveRequestList(2l).size());
    }
    @Test
    public void testResponseAnnualLeaveRequest(){
        AnnualLeaveRequestResponseDto dto=new AnnualLeaveRequestResponseDto();
        dto.setRequestID(1l);
        dto.setResponderEmployeeID(1l);
        dto.setStatus(RequestStatus.DECLINED);

        AnnualLeaveRequest req=new AnnualLeaveRequest();
        req.setId(1l);
        Employee emp = new Employee();
        emp.setId(2l);
        Department dep=new Department();
        dep.setId(2l);
        Employee manager = new Employee();
        manager.setId(1l);
        dep.setManager(manager);
        emp.setDepartment(dep);
        req.setRequestedBy(emp);

        when(annualLeaveRequestDao.findByIdAndStatus(anyLong(),any(RequestStatus.class)))
                .thenReturn(req);

        AnnualLeaveTransaction tx=new AnnualLeaveTransaction();
        tx.setEmployee(emp);
        tx.setLeaveTransactionAmount(10);

        when(annualLeaveTransactionDao.findByID(anyLong())).thenReturn(tx);
        ArrayList<AnnualLeaveRequest>requests=new ArrayList<>();
        when(annualLeaveRequestDao.findWaitingRequestListByDepartmentID(anyLong()))
                .thenReturn(requests);

        annualLeaveService.responseAnnualLeaveRequest(dto);

        assertEquals(0,annualLeaveRequestDao.findWaitingRequestListByDepartmentID(anyLong()).size());

    }
}
