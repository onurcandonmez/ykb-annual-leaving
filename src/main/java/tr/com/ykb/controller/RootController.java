package tr.com.ykb.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

@Controller
@Path("/")
public class RootController {

    @GET
    @Path("/")
    public Response getReq() throws URISyntaxException {
        return Response.temporaryRedirect(new URI("swagger-ui.html")).build();
    }
}
