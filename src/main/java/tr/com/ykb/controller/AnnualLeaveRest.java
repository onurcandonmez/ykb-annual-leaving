package tr.com.ykb.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tr.com.ykb.dto.AnnualLeaveRequestDto;
import tr.com.ykb.dto.AnnualLeaveRequestResponseDto;
import tr.com.ykb.dto.RestResponse;
import tr.com.ykb.exception.ServiceException;
import tr.com.ykb.model.AnnualLeaveRequest;
import tr.com.ykb.model.Employee;
import tr.com.ykb.service.AnnualLeaveService;

import javax.ws.rs.core.MediaType;
import javax.xml.ws.Response;
import java.util.List;

@RestController
@RequestMapping("annual_leave")
@Api(value = "Employee leave request Controller" ,produces = MediaType.APPLICATION_JSON)
public class AnnualLeaveRest {


    @Autowired
    private AnnualLeaveService annualLeaveService;

    @PostMapping(value = "/request",produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Make annual leave request", notes = "Send annual leave request to manager", response = AnnualLeaveRequest.class)
    public RestResponse<AnnualLeaveRequest> makeAnnualLeaveRequest(@RequestBody AnnualLeaveRequestDto requestObject) {
        AnnualLeaveRequest _req=annualLeaveService.requestAnnualService(requestObject);
        RestResponse<AnnualLeaveRequest> resp= new RestResponse<AnnualLeaveRequest>();
        resp.setData(_req);
        return resp;
    }

    @GetMapping(value = "/request/{employeeID}",produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get annual leave requests of employees which you are the manager", notes = "", response = AnnualLeaveRequest.class)
    public RestResponse<List<AnnualLeaveRequest>> getAnnualLeaveRequestList(@PathVariable("employeeID") Long employeeID) {
        List<AnnualLeaveRequest> requestList=annualLeaveService.getWaitingAnnualLeaveRequestList(employeeID);
        RestResponse<List<AnnualLeaveRequest>> resp= new RestResponse<>();
        resp.setData(requestList);
        return resp;
    }

    @PostMapping(value = "/response",produces = MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Response annual leave requests", notes = "Send response annual leave requests", response = AnnualLeaveRequest.class)
    public RestResponse<List<AnnualLeaveRequest>> responseAnnualLeaveRequest(@RequestBody AnnualLeaveRequestResponseDto responseObject) {
        List<AnnualLeaveRequest> requestList=annualLeaveService.responseAnnualLeaveRequest(responseObject);
        RestResponse<List<AnnualLeaveRequest>> resp= new RestResponse<>();
        resp.setData(requestList);
        return resp;
    }
}
