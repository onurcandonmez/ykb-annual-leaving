package tr.com.ykb.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tr.com.ykb.model.AnnualLeaveRequest;
import tr.com.ykb.model.AnnualLeaveRule;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class AnnualLeaveRuleDaoImpl implements AnnualLeaveRuleDao {

    @Autowired
    private EntityManager em;

    @Override
    public AnnualLeaveRule getAnnualLeaveRuleWithWorkedMonth(int workingYearPast) {

        List<AnnualLeaveRule> annualLeaveRuleList= em.createQuery("from AnnualLeaveRule a where totalWorkYear =: year")
                .setParameter("year",workingYearPast).getResultList();

        if(annualLeaveRuleList.size()>0)
            return annualLeaveRuleList.get(0);
        else
            return null;
    }

    @Override
    public AnnualLeaveRule findByWorkYear(int workedYear) {
        List<AnnualLeaveRule> annualLeaveRuleList =em.createQuery("from AnnualLeaveRule where totalWorkYear=:year")
                .setParameter("year",workedYear).getResultList();

        if(annualLeaveRuleList.size()>0)
            return annualLeaveRuleList.get(0);
        else
            return null;
    }
}
