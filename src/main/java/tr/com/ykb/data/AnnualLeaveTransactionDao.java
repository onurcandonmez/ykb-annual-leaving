package tr.com.ykb.data;

import org.springframework.data.repository.CrudRepository;
import tr.com.ykb.model.AnnualLeaveTransaction;

import javax.persistence.Entity;

public interface AnnualLeaveTransactionDao {
    public void save(AnnualLeaveTransaction annualLeaveTransaction);

   public Long getTotalLeaveAmountByEmployeeID(Long id);

    public AnnualLeaveTransaction findByID(Long requestID);
}
