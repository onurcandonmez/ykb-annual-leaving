package tr.com.ykb.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tr.com.ykb.model.AnnualLeaveRequest;
import tr.com.ykb.model.RequestStatus;

import javax.persistence.EntityManager;
import java.util.Date;
import java.util.List;

@Repository
public class AnnualLeaveRequestDaoImpl implements AnnualLeaveRequestDao {

    @Autowired
    private EntityManager em;

    @Override
    public List<AnnualLeaveRequest> getLeaveRequestListByEmployeeIDAndStatus(Long employeeID, RequestStatus status) {
        List requestList=em.createQuery("from AnnualLeaveRequest where requestedBy.id =:id and status =:status")
                .setParameter("id",employeeID)
                .setParameter("status",status).getResultList();

        return requestList;
    }

    @Override
    public List<AnnualLeaveRequest> getRequestListWithOutDeclined(Long employeeID) {
        List requestList=em.createQuery("from AnnualLeaveRequest where requestedBy.id =:id and status != :status")
                .setParameter("id",employeeID)
                .setParameter("status",RequestStatus.DECLINED).getResultList();

        return requestList;
    }

    @Override
    public void save(AnnualLeaveRequest req) {
        em.persist(req);
    }

    @Override
    public boolean hasOverlapRequestDate(Date startDate, Date endDate, Long employeeID) {
        List<AnnualLeaveRequest> requests=em.createQuery("from AnnualLeaveRequest where requestedBy.id=:id and :endDate > startDate and endDate > :startDate")
                .setParameter("endDate",endDate).setParameter("startDate",startDate).setParameter("id",employeeID)
                .getResultList();
        if(requests.size()>0)
            return true;
        else
            return false;
    }

    @Override
    public List<AnnualLeaveRequest> findWaitingRequestListByDepartmentID(Long departmentID) {
        List<AnnualLeaveRequest> requestList= em.createQuery("from AnnualLeaveRequest where requestedBy.department.id=:depID and status=:status")
                .setParameter("depID",departmentID).setParameter("status",RequestStatus.WAITING).getResultList();
        return requestList;
    }

    @Override
    public AnnualLeaveRequest findById(Long requestID) {
        String query="from AnnualLeaveRequest where id =: id";
        List<AnnualLeaveRequest> requestList =em.createQuery(query).setParameter("id",requestID).getResultList();
        if(requestList.size()>0)
            return requestList.get(0);
        else
            return null;
    }

    @Override
    public void updateRequest(AnnualLeaveRequest req) {
        em.merge(req);
    }

    @Override
    public AnnualLeaveRequest findByIdAndStatus(Long requestID, RequestStatus requestStatus) {
        String query="from AnnualLeaveRequest where id =: id and status =:status";
        List<AnnualLeaveRequest> requestList =em.createQuery(query).setParameter("id",requestID)
                .setParameter("status",requestStatus).getResultList();
        if(requestList.size()>0)
            return requestList.get(0);
        else
            return null;
    }
}
