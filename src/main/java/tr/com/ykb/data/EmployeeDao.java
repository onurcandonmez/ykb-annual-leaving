package tr.com.ykb.data;

import org.springframework.stereotype.Repository;
import tr.com.ykb.model.Employee;

import java.util.List;

public interface EmployeeDao {
    Employee findByID(Long employeeID);
    List<Employee> getAnnualLeaveMustBeCheckList();
    void update(Employee employee);
}
