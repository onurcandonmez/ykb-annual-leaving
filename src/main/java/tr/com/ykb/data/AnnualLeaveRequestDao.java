package tr.com.ykb.data;

import tr.com.ykb.dto.AnnualLeaveRequestDto;
import tr.com.ykb.model.AnnualLeaveRequest;
import tr.com.ykb.model.RequestStatus;

import java.util.Date;
import java.util.List;

public interface AnnualLeaveRequestDao {

    public List<AnnualLeaveRequest> getLeaveRequestListByEmployeeIDAndStatus(Long employeeID, RequestStatus requestStatus);

    public List<AnnualLeaveRequest> getRequestListWithOutDeclined(Long employeeID);

    void save(AnnualLeaveRequest req);

    public boolean hasOverlapRequestDate(Date startDate, Date endDate,Long employeeID);

    List<AnnualLeaveRequest> findWaitingRequestListByDepartmentID(Long departmentID);

    AnnualLeaveRequest findById(Long requestID);

    void updateRequest(AnnualLeaveRequest req);

    AnnualLeaveRequest findByIdAndStatus(Long requestID, RequestStatus requestStatus);
}
