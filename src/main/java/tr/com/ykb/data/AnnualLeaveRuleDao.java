package tr.com.ykb.data;

import tr.com.ykb.model.AnnualLeaveRequest;
import tr.com.ykb.model.AnnualLeaveRule;

import java.util.List;

public interface AnnualLeaveRuleDao {
    public AnnualLeaveRule getAnnualLeaveRuleWithWorkedMonth(int workingYearPast);
    AnnualLeaveRule findByWorkYear(int workedYear);

}
