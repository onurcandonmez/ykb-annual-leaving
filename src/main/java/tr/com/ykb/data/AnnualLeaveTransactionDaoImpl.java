package tr.com.ykb.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tr.com.ykb.model.AnnualLeaveTransaction;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class AnnualLeaveTransactionDaoImpl implements AnnualLeaveTransactionDao {
    @Autowired
    private EntityManager em;

    @Override
    public void save(AnnualLeaveTransaction annualLeaveTransaction) {
        em.persist(annualLeaveTransaction);
    }

    @Override
    public Long getTotalLeaveAmountByEmployeeID(Long id) {
        Long totalLeaveAmount= (Long) em.createQuery("select sum(leaveTransactionAmount) from AnnualLeaveTransaction where employee.id =: id")
                .setParameter("id",id).getSingleResult();
        return totalLeaveAmount;
    }

    @Override
    public AnnualLeaveTransaction findByID(Long requestID) {
        String query="from AnnualLeaveTransaction where request.id=:reqID";
        List<AnnualLeaveTransaction> txList= em.createQuery(query).setParameter("reqID",requestID).getResultList();
        if(txList.size()>0)
            return txList.get(0);
        else
            return null;
    }
}
