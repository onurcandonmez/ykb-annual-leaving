package tr.com.ykb.data;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import tr.com.ykb.model.AnnualLeaveTransaction;
import tr.com.ykb.model.Employee;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class EmployeeDaoImpl implements EmployeeDao{
    @Autowired
    private EntityManager em;

    @Override
    public Employee findByID(Long employeeID) {
        List<Employee> employees =em.createQuery("from Employee where id =:id")
                .setParameter("id",employeeID).getResultList();
        if(employees.size()>0)
            return  employees.get(0);
        else
            return null;
    }

    @Override
    public List<Employee> getAnnualLeaveMustBeCheckList() {


        List<Employee> employees = em.createQuery("from Employee").getResultList();

        for (Employee employee : employees) {
            for(AnnualLeaveTransaction tx:employee.getTransactionList()){
            }
        }
        return employees;
    }

    @Override
    public void update(Employee employee) {
        em.merge(employee);
    }
}
