package tr.com.ykb.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.JoinColumnOrFormula;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String identityNo;
    private Date birthDate;
    private Date startDate;
    private Date creationDate;
    private Integer workedYear;

    @OneToMany(mappedBy = "employee")
    @JsonIgnore
    private List<AnnualLeaveTransaction> transactionList;

    @OneToMany(mappedBy = "requestedBy")
    @JsonIgnore
    private List<AnnualLeaveRequest> leaveRequests;

    @ManyToOne
    @JoinColumn(name="department_id")
    @JsonIgnore
    private Department department;

    @ManyToOne
    @JoinColumn(name="country_id")
    private Country citizenOfCountry;

}
