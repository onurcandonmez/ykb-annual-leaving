package tr.com.ykb.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

    @ManyToOne
    @JoinColumn(name = "manager_employee_id")
    private Employee manager;

    @OneToMany(mappedBy = "department")
    private List<Employee> employeeList;

    @ManyToOne
    @JoinColumn(name = "top_department_id")
    private Department topDepartment;

    @OneToMany(mappedBy = "topDepartment")
    private List<Department> subDepartmentList;

    private Date creationDate;

}
