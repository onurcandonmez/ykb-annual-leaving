package tr.com.ykb.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Getter
@Setter
public class Country {
    @Id
    private Long id;
    private String countryName;
    private String twoCharCountryCode;
    private String treeCharCountryCode;

}
