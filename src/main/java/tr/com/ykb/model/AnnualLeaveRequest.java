package tr.com.ykb.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class AnnualLeaveRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "employeeID")
    private Employee requestedBy;
    private Date startDate;
    private Date endDate;
    private int leaveAsWorkingDay;
    @Enumerated(EnumType.STRING)
    private RequestStatus status;

    private Date creationDate;
    private Date updatedDate;
}
