package tr.com.ykb.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class AnnualLeaveTransaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "employeeID")
    private Employee employee;
    private Integer leaveTransactionAmount;
    private Date creationDate;
    @ManyToOne
    @JoinColumn(name="requestID")
    private AnnualLeaveRequest request;
}
