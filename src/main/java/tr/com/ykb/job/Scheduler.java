package tr.com.ykb.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import tr.com.ykb.service.AnnualLeaveService;

import javax.annotation.PostConstruct;

@Component
public class Scheduler {

    @Autowired
    private AnnualLeaveService annualLeaveService;

    @PostConstruct
    private void firstRun(){
        employeeAnnualLeaveChecker();
    }
    @Scheduled(cron = "0 0 1 * * *")
    public void employeeAnnualLeaveChecker() {
        annualLeaveService.updateEmployeeAnnualLeaves();
    }
}
