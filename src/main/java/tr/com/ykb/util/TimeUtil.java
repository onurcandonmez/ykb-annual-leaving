package tr.com.ykb.util;

import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.joda.time.Years;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class TimeUtil {


//    public static void main(String[] args) {
//        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
//
//        try {
//            int difference = monthsBetweenDates(sdf.parse("2019-02-24"),sdf.parse("2022-10-22"));
//            System.out.println(difference);
//
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//    }

    public static int monthsBetweenDates(Date startDate, Date endDate){

        int monthsBetween = Months.monthsBetween( LocalDate.fromDateFields(startDate),  LocalDate.fromDateFields(endDate))
                .getMonths();
        return monthsBetween;
    }

    public static int yearsBetweenDates(Date startDate,Date endDate){
        int yearBetween = Years.yearsBetween(LocalDate.fromDateFields(startDate),LocalDate.fromDateFields(endDate))
                .getYears();
        return yearBetween;
    }
    public static boolean isSameDay(Date date1,Date date2){
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
        return  sameDay;
    }
}
