package tr.com.ykb.service;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import tr.com.ykb.util.TimeUtil;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class WorkingDayService {

    private List<Date> holidaysInTwoYear;

    @Value("${calendarific.api.key}")
    private String calendarificApiKey;

    @Autowired
    private RestTemplate restTemplate;

    private SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");

    @PostConstruct
    private void loadHolidays() throws URISyntaxException {

        holidaysInTwoYear=new ArrayList<Date>();

        Calendar calendar=GregorianCalendar.getInstance();
        Integer year=calendar.get(Calendar.YEAR);

        fillHolidayList(year);
        fillHolidayList(year+1);

    }

    private void fillHolidayList(int year) {
        String uri= String.format("https://calendarific.com/api/v2/holidays?api_key=%s&year=%d&country=%s",
                calendarificApiKey,year,"TR");
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

            ResponseEntity<String> response = restTemplate.exchange(uri, HttpMethod.GET,entity, String.class);
            JSONObject holidaysJSONObject =new JSONObject(response.getBody());

            JSONArray holidays=holidaysJSONObject.getJSONObject("response").getJSONArray("holidays");
            holidays.forEach(holiday -> {
                String date=((JSONObject)holiday).getJSONObject("date").getString("iso");
                try {
                    holidaysInTwoYear.add(sdf.parse(date));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            } );
        } catch (Exception ex) {
            ex.printStackTrace();

        }
    }

    public int getWorkingDayCount(Date startDate, Date endDate) {
        int workingDays = 0;
        try
        {
            Calendar start = Calendar.getInstance();
            start.setTime(startDate);

            Calendar end = Calendar.getInstance();
            end.setTime(endDate);

            while(!start.after(end))//removed ; (semi-colon)
            {
                //int day = start.getDay();
                int day = start.get(Calendar.DAY_OF_WEEK);
                if ((day != Calendar.SATURDAY)
                        && (day != Calendar.SUNDAY)
                        && (isHoliday(start.getTime())==false))
                    workingDays++;

                start.add(Calendar.DATE, 1);//removed comment tags
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return  workingDays;
    }

    private boolean isHoliday(Date date) {
        for (Date holiday : getHolidaysInTwoYear()) {
            boolean isSameDay = TimeUtil.isSameDay(date, holiday);
            if(isSameDay)
                return true;
        }
        return false;
    }
    public void setHolidaysInTwoYear(List<Date> holidays){
        this.holidaysInTwoYear = holidays;
    }
    public List<Date> getHolidaysInTwoYear(){
        return this.holidaysInTwoYear;
    }
}
