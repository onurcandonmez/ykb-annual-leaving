package tr.com.ykb.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tr.com.ykb.data.AnnualLeaveRequestDao;
import tr.com.ykb.data.AnnualLeaveRuleDao;
import tr.com.ykb.data.AnnualLeaveTransactionDao;
import tr.com.ykb.data.EmployeeDao;
import tr.com.ykb.dto.AnnualLeaveRequestDto;
import tr.com.ykb.dto.AnnualLeaveRequestResponseDto;
import tr.com.ykb.exception.ServiceException;
import tr.com.ykb.model.*;
import tr.com.ykb.util.Check;
import tr.com.ykb.util.TimeUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class AnnualLeaveService {

    @Autowired
    private EmployeeDao employeeDao;

    @Autowired
    private AnnualLeaveRuleDao annualLeaveRuleDao;

    @Autowired
    private WorkingDayService workingDayService;

    @Autowired
    private AnnualLeaveRequestDao annualLeaveRequestDao;

    @Autowired
    private AnnualLeaveTransactionDao annualLeaveTransactionDao;

    SimpleDateFormat sdf =new SimpleDateFormat("yyyy-MM-dd");
    @Transactional
    public AnnualLeaveRequest requestAnnualService(AnnualLeaveRequestDto req) {
        log.debug("Employee requested annual leave. EmployeeID: {} StartDate: {} EndDate: {}",req.getEmployeeID(),sdf.format(req.getStartDate()),sdf.format(req.getEndDate()));

        Check.notNull(req, "Exception.nullRequest", "body");
        Check.notNull(req.getEmployeeID(), "Exception.nullRequest", "employeeID");
        Check.notNull(req.getStartDate(), "Exception.nullRequest", "startDate");
        Check.notNull(req.getEndDate(), "Exception.nullRequest", "endDate");
        Check.isFalse(TimeUtil.isSameDay(req.getStartDate(),req.getEndDate()),"Exception.sameDay");
        if (req.getStartDate().getTime() > req.getEndDate().getTime())
            throw new ServiceException("Exception.endDateMustGreater", null);

        Employee emp = employeeDao.findByID(req.getEmployeeID());

        Check.notNull(emp, "Exception.employeeNotFound", req.getEmployeeID());

        int wantedToLeaveWorkingDayCount = workingDayService.getWorkingDayCount(req.getStartDate(), req.getEndDate());
        Long totalLeaveRight = annualLeaveTransactionDao.getTotalLeaveAmountByEmployeeID(emp.getId());
        if (totalLeaveRight < wantedToLeaveWorkingDayCount)
            throw new ServiceException("Exception.notEnoughLeave", new Object[]{totalLeaveRight});

        boolean hasOverlappingDates = annualLeaveRequestDao.hasOverlapRequestDate(req.getStartDate(), req.getEndDate(),emp.getId());
        if(hasOverlappingDates)
            throw new ServiceException("Exception.overlappingDates",null);

        AnnualLeaveRequest _req = new AnnualLeaveRequest();
        _req.setCreationDate(new Date());
        _req.setUpdatedDate(new Date());
        _req.setStartDate(req.getStartDate());
        _req.setEndDate(req.getEndDate());
        _req.setRequestedBy(emp);
        _req.setStatus(RequestStatus.WAITING);
        _req.setLeaveAsWorkingDay(wantedToLeaveWorkingDayCount);

        annualLeaveRequestDao.save(_req);

        AnnualLeaveTransaction tx = new AnnualLeaveTransaction();
        tx.setLeaveTransactionAmount(-1 * wantedToLeaveWorkingDayCount);
        tx.setEmployee(emp);
        tx.setCreationDate(new Date());
        tx.setRequest(_req);
        annualLeaveTransactionDao.save(tx);

        return _req;
    }

    @Transactional
    public void updateEmployeeAnnualLeaves() {
        log.debug("Employee annual leave updater job started");

        List<Employee> wllBeCheckEmployeeList = employeeDao.getAnnualLeaveMustBeCheckList();
        for (Employee employee : wllBeCheckEmployeeList) {
            int workedYear = TimeUtil.yearsBetweenDates(employee.getStartDate(), new Date());

            if (employee.getWorkedYear() == null || employee.getWorkedYear() < workedYear) {
                AnnualLeaveRule rule = annualLeaveRuleDao.findByWorkYear(workedYear);
                AnnualLeaveTransaction tx = new AnnualLeaveTransaction();
                tx.setCreationDate(new Date());
                tx.setEmployee(employee);
                tx.setLeaveTransactionAmount(rule.getAnnualLeaveDayCount());
                annualLeaveTransactionDao.save(tx);

                employee.setWorkedYear(workedYear);
                employeeDao.update(employee);
            }
        }
    }

    public List<AnnualLeaveRequest> getWaitingAnnualLeaveRequestList(Long employeeID) {

        log.debug("Employee requested annual leave requests. EmployeeID: {}",employeeID);

        Employee emp = employeeDao.findByID(employeeID);
        Check.notNull(emp, "Exception.employeeNotFound", employeeID);
        Check.notNull(emp.getDepartment(),"Exception.departmentNotFound");
        Check.isTrue(emp.getDepartment().getManager().getId()==employeeID,"Exception.departmentNotFound");

        List<AnnualLeaveRequest> requestList =annualLeaveRequestDao.findWaitingRequestListByDepartmentID(emp.getDepartment().getId());
        return requestList;
    }

    @Transactional
    public List<AnnualLeaveRequest> responseAnnualLeaveRequest(AnnualLeaveRequestResponseDto resp) {

        log.debug("Employee respond annual leave request. EmployeeID: {} RequestID: {}",resp.getResponderEmployeeID(),resp.getRequestID());

        Check.notNull(resp.getRequestID(),"Exception.nullRequest","requestID");
        Check.notNull(resp.getResponderEmployeeID(),"Exception.nullRequest","responderEmployeeID");
        Check.notNull(resp.getStatus(),"Exception.nullRequest","status");
        Check.isTrue(resp.getStatus().equals(RequestStatus.DECLINED)|| resp.getStatus().equals(RequestStatus.APPROVED),
                "Exception.invalidResponse","status");

        AnnualLeaveRequest req=annualLeaveRequestDao.findByIdAndStatus(resp.getRequestID(),RequestStatus.WAITING);
        Check.notNull(req,"Exception.objectNotFound","AnnualLeaveRequest");

        boolean managerIsTrue=(req.getRequestedBy().getDepartment().getManager().getId()==resp.getResponderEmployeeID());
        Check.isTrue(managerIsTrue,"Exception.departmentNotFound");
        boolean isSameEmployee =req.getRequestedBy().getId().equals(resp.getResponderEmployeeID());
        Check.isTrue(isSameEmployee == false ,"Exception.selfRequestResponse");

        req.setUpdatedDate(new Date());
        req.setStatus(resp.getStatus());
        annualLeaveRequestDao.updateRequest(req);


        if(resp.getStatus().equals(RequestStatus.DECLINED)){
            AnnualLeaveTransaction tx=annualLeaveTransactionDao.findByID(req.getId());
            AnnualLeaveTransaction txForResp=new AnnualLeaveTransaction();
            txForResp.setCreationDate(new Date());
            txForResp.setEmployee(tx.getEmployee());
            txForResp.setRequest(tx.getRequest());
            txForResp.setLeaveTransactionAmount(tx.getLeaveTransactionAmount() * -1);
            annualLeaveTransactionDao.save(txForResp);
        }


        List<AnnualLeaveRequest> requestList =annualLeaveRequestDao.findWaitingRequestListByDepartmentID(req.getRequestedBy().getDepartment().getId());
        return requestList;
    }
}
