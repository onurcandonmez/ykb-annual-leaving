package tr.com.ykb.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import tr.com.ykb.model.RequestStatus;

import java.util.Date;

@Getter
@Setter
public class AnnualLeaveRequestDto {
    private Date startDate;
    private Date endDate;
    private Long employeeID;
}
