package tr.com.ykb.dto;

import lombok.Getter;
import lombok.Setter;
import tr.com.ykb.model.RequestStatus;
@Getter
@Setter
public class AnnualLeaveRequestResponseDto {
    private Long responderEmployeeID;
    private Long requestID;
    private RequestStatus status;
}
