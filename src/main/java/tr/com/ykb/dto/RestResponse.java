package tr.com.ykb.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter // All lombok annotations
@Getter
@ToString
public class RestResponse<T> {

    private String status;
    private Integer httpcode;
    private String devMessage;
    private String userMessage;
    private T data;
}
